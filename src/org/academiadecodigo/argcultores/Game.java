package org.academiadecodigo.argcultores;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import javax.swing.text.Position;
import java.util.Random;


public class Game extends Object implements KeyboardHandler {

    private Picture background;
    private Picture player;
    private Picture[] beer;
    private int qndBeer;
    final static int PADDING = 10;
    public static int count;
    private boolean collisionDetected;
    private boolean play;

    public Game() {

        // background
        background = new Picture(PADDING, PADDING, "resources/background.png");
        background.draw();

        // my image
        player = new Picture(400, 150, "resources/player.png");
        player.draw();

        // play
        play = true;

        qndBeer = 500;
        // array of beer
        this.beer = new Picture[qndBeer];

        collisionDetected = false;

        keyboardInit();
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_SPACE:
                try {
                    this.play = true;
                    start();
                } catch (InterruptedException exception) {

                }
                break;
            case KeyboardEvent.KEY_UP:
                player.translate(0, -20);
                break;
            case KeyboardEvent.KEY_DOWN:
                player.translate(0, 20);
                break;
            case KeyboardEvent.KEY_RIGHT:
                player.translate(20, 0);
                break;
            case KeyboardEvent.KEY_LEFT:
                player.translate(-20, 0);
                break;
        }
    }

    public void keyboardInit() {
        Keyboard keyboard = new Keyboard(this);

        KeyboardEvent up = new KeyboardEvent();
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        up.setKey(KeyboardEvent.KEY_UP);
        keyboard.addEventListener(up);

        KeyboardEvent down = new KeyboardEvent();
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        down.setKey(KeyboardEvent.KEY_DOWN);
        keyboard.addEventListener(down);

        KeyboardEvent right = new KeyboardEvent();
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        right.setKey(KeyboardEvent.KEY_RIGHT);
        keyboard.addEventListener(right);

        KeyboardEvent left = new KeyboardEvent();
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        left.setKey(KeyboardEvent.KEY_LEFT);
        keyboard.addEventListener(left);

        KeyboardEvent space = new KeyboardEvent();
        space.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        space.setKey(KeyboardEvent.KEY_SPACE);
        keyboard.addEventListener(space);
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

    public void start() throws InterruptedException {

        blockCreator();
        while (play) {
            moveBlocks();
        }
        play = true;
        start();
    }

    public void blockCreator() {

        for (int i = 0; i < this.beer.length; i++) {
            Random r = new Random();
            int rand = r.nextInt(20,300);

            this.beer[i] = new Picture(800, rand, "resources/beer.png");
            this.beer[i].draw();
        }
    }

    public void moveBlocks() throws InterruptedException {

        int count = 0;
        int speed = 100;

        for (int i = 0; i < this.beer.length; i++) {

            while (this.beer[i].getX() > 50) {

                this.beer[i].translate(-20, 0); // move left
                System.out.println(this.beer[i].getX());

                if ((player.getX() <= beer[i].getX()) || (player.getY() == beer[i].getY())) {
                    this.beer[i].delete();
                }
                if (count > 3) {
                    speed = 50;
                }
                Thread.sleep(speed);
            }
        }
        this.play = false;
    }


    public int colunToX(int col) {
        return PADDING * col;
    }

    public int rowToY(int row) {
        return PADDING * row;
    }

}
